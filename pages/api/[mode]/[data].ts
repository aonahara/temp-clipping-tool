import { NextApiRequest, NextApiResponse } from "next";
import ytdl from "ytdl-core";
import { generateCommand } from "../../../modules/utils/generateCommand";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { mode, data } = req.query;
  const { url, timestamps, output } = JSON.parse(
    Buffer.from(data as string, "base64").toString()
  );

  // Fetch video info
  const info = await ytdl.getInfo(url as string);
  const bestVideo = info.formats
    .filter((fmt) => fmt.hasVideo && !fmt.hasAudio)
    .sort((a, b) => b.height - a.height)
    .sort((a, b) => b.fps - a.fps)[0];
  const bestAudio = info.formats
    .filter((fmt) => fmt.hasAudio && !fmt.hasVideo)
    .sort((a, b) => b.bitrate - a.bitrate)[0];

  // Generate command
  const command = generateCommand({
    url: url as string,
    timestamps: timestamps as string,
    fmtVideo: bestVideo.itag.toString(),
    fmtAudio: bestAudio.itag.toString(),
    videoResolution: bestVideo.width + "x" + bestVideo.height,
    videoFramerate: bestVideo.fps.toString(),
    outputArguments: output,
    platform: mode === "powershell" ? "powershell" : "bash",
  });

  let prerun = "";
  if (mode === "colab")
    prerun += `wget http://youtube-dl.org/downloads/latest/youtube-dl -O /usr/bin/youtube-dl && chmod +x /usr/bin/youtube-dl`;

  res.setHeader("Content-Type", "text/x-shellscript");
  res.writeHead(200);
  res.write(`#!/bin/bash\n${prerun}\n` + command + "\n");
  res.end();
};
