import { NextApiRequest, NextApiResponse } from "next";
import ytdl from "ytdl-core";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { url } = req.query;
  const info = await ytdl.getInfo(url as string);
  const video = info.formats
    .filter((fmt) => fmt.hasVideo && !fmt.hasAudio)
    .sort((a, b) => b.height - a.height)
    .sort((a, b) => b.fps - a.fps)[0];
  const audio = info.formats
    .filter((fmt) => fmt.hasAudio && !fmt.hasVideo)
    .sort((a, b) => b.bitrate - a.bitrate)[0];
  res.status(200).json({ video, audio });
};
