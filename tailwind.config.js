module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: ["./pages/**/*.js", "./pages/**/*.tsx", "./modules/**/*.tsx"],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
