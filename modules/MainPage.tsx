import React from "react";

const MainPage = () => {
  const [url, setURL] = React.useState("");
  const [timestamps, setTimestamps] = React.useState("");
  const [output, setOutput] = React.useState("-c copy output.mkv");
  const [mode, setMode] = React.useState<"bash" | "colab" | "powershell">(
    "colab"
  );
  const [generatedCommand, setGeneratedCommand] = React.useState("");

  React.useEffect(() => {
    const scriptURL = `https://temp-clipping-tool.vercel.app/api/${mode}/${encodeURIComponent(
      btoa(JSON.stringify({ url, timestamps, output }))
    )}`;

    if (mode === "powershell") {
      setGeneratedCommand(
        `Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('${scriptURL}'))`
      );
    } else {
      setGeneratedCommand(
        `${mode === "colab" ? "!" : ""}curl ${scriptURL} | bash`
      );
    }
  }, [url, timestamps, mode, output]);

  const CLASS_INPUT =
    "appearance-none shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline";
  const CLASS_LABEL =
    "block text-sm font-medium leading-5 text-gray-700 mb-1 mt-2";
  const CLASS_BUTTON =
    "border shadow py-2 px-3 mr-2 mt-4 bg-white rounded hover:shadow-lg transition duration-200 active:shadow-none focus:outline-none focus:shadow-outline text-gray-900 text-sm";

  return (
    <div className="bg-gray-200 min-h-screen">
      <div className="container mx-auto p-16">
        <h1 className="text-xl font-bold text-gray-700">
          ffmpeg command generator
        </h1>
        <p className="text-sm font-light text-gray-500 mb-4">
          made with 🍝 by kitsune
        </p>

        <div className="block bg-blue-500 text-white py-4 px-6 rounded-lg mb-4">
          <b>Important change</b>
          <p>
            This tool will now automatically add the 5-second padding before and
            after every timestamp. It will also merge overlapping timestmaps.
            You don't need to manually adjust the timestamps now as the tool
            will do it for you.
          </p>
        </div>

        <label htmlFor="url" className={CLASS_LABEL}>
          Enter video URL
        </label>
        <input
          id="url"
          className={CLASS_INPUT}
          placeholder="https://www.youtube.com/watch?v=dQw4w9WgXcQ"
          value={url}
          onChange={(e) => setURL(e.target.value)}
        />

        <label htmlFor="ts" className={CLASS_LABEL}>
          Timestamps
        </label>
        <input
          id="ts"
          className={CLASS_INPUT}
          placeholder="[0:00 - 1:23] [4:56 - 7:01]"
          value={timestamps}
          onChange={(e) => setTimestamps(e.target.value)}
        />

        <label htmlFor="output" className={CLASS_LABEL}>
          Output flags
        </label>
        <input
          id="output"
          className={CLASS_INPUT + " font-mono"}
          value={output}
          onChange={(e) => setOutput(e.target.value)}
        />

        <button
          type="button"
          className={CLASS_BUTTON}
          onClick={() => setMode("colab")}
        >
          Colab
        </button>
        <button
          type="button"
          className={CLASS_BUTTON}
          onClick={() => setMode("bash")}
        >
          Bash
        </button>
        <button
          type="button"
          className={CLASS_BUTTON}
          onClick={() => setMode("powershell")}
        >
          Powershell
        </button>

        <h2 className="block text-sm font-medium leading-5 text-gray-700 mb-1 mt-4">
          Resulting {mode} command
        </h2>
        <input
          readOnly
          className={CLASS_INPUT}
          onClick={(e) => e.currentTarget.select()}
          value={generatedCommand}
        />

        <div className="mt-4">
          <h2 className="block text-sm font-medium leading-5 text-gray-700 mb-1 mt-4">
            Additional notes
          </h2>
          {mode === "colab" ? (
            <ul className="list-disc ml-4 text-sm text-gray-700 font-light">
              <li>
                Create a new notebook at{" "}
                <a
                  className="text-blue-600"
                  href="https://colab.research.google.com/#create=true"
                >
                  https://colab.research.google.com/
                </a>
              </li>
              <li>Paste the command above inside the box</li>
              <li>Hit Ctrl+Enter to run the code</li>
              <li>
                If you get an error saying "YouTube said: Unable to extract
                video data", try again and it should work
              </li>
              <li>
                Once finished, your video should be visible in the files panel
                on the left
              </li>
            </ul>
          ) : mode === "bash" ? (
            <ul className="list-disc ml-4 text-sm text-gray-700 font-light">
              <li>
                Make sure you have curl, ffmpeg, youtube-dl, and python3
                installed
              </li>
              <li>Make sure all those programs are in your $PATH</li>
              <li>
                If youtube-dl complains about missing python, even though you
                have it installed, try running{" "}
                <code>sudo ln -s /usr/bin/python3 /usr/bin/python</code>
              </li>
              <li>
                If you get an error saying "YouTube said: Unable to extract
                video data", try again and it should work
              </li>
            </ul>
          ) : mode === "powershell" ? (
            <ul className="list-disc ml-4 text-sm text-gray-700 font-light">
              <li>
                Make sure you have curl, ffmpeg, youtube-dl, and python3
                installed
              </li>
              <li>Make sure all those programs are in your $PATH</li>
              <li>
                If you get an error saying "YouTube said: Unable to extract
                video data", try again and it should work
              </li>
            </ul>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};

export default MainPage;
