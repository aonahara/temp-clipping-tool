import {
  mergeTimestamps,
  padTimestamps,
  insertBlanks,
  blankedTimestampsToString,
} from "./timestamp-tools";
import { parseTimestampPairs } from "./timestampParser";

type GenerateCommandArgs = {
  url: string;
  fmtVideo: string;
  fmtAudio: string;
  videoResolution: string;
  videoFramerate: string;
  timestamps: string;
  outputArguments: string;
  platform: "powershell" | "bash";
};

const randomString = () => Math.random().toString(36).substring(2);

export const generateCommand = (args: GenerateCommandArgs) => {
  const {
    url,
    fmtVideo,
    fmtAudio,
    videoResolution,
    videoFramerate,
    timestamps,
    outputArguments,
    platform,
  } = args;
  const K_SILENT_DURATION = 3;
  const prefix = randomString();

  const ts = parseTimestampPairs(timestamps);
  const tsb = mergeTimestamps(padTimestamps(insertBlanks(ts, 10)));
  const unquotedURL = url.replace(/"/g, '\\"');

  // Keep track of temporary files to delete
  const filesToRemove = [];

  // Show video info
  const info = `
echo "Downloading video: ${unquotedURL}"
echo "With timestamps: ${blankedTimestampsToString(tsb)}"
echo "With output flags: ${outputArguments.replace(/"/g, '\\"')}"
`;

  let ffmpeg = "ffmpeg";
  if (platform === "bash") ffmpeg = "< /dev/null ffmpeg";

  // Define variables
  let getUrls = "";
  if (platform === "bash") {
    getUrls = `set -e
echo "Fetching URLs (1 of 2)"
export VID=$(youtube-dl -f ${fmtVideo} -g "${unquotedURL}")
echo "Fetching URLs (2 of 2)"
export AUD=$(youtube-dl -f ${fmtAudio} -g "${unquotedURL}")
`;
  } else if (platform === "powershell") {
    getUrls = `$ErrorActionPreference = "Stop"
echo "Fetching URLs (1 of 2)"
$VID = ((youtube-dl -f ${fmtVideo} -g "${unquotedURL}") | Out-String).Trim()
echo "Fetching URLs (2 of 2)"
$AUD = ((youtube-dl -f ${fmtAudio} -g "${unquotedURL}") | Out-String).Trim()
`;
  }

  // Script to generate blank video
  const BLANK_FILE = prefix + "blank.ts";
  filesToRemove.push(BLANK_FILE);
  let generateBlank = `echo "Generating blank video"
${ffmpeg} -hide_banner -loglevel fatal -f lavfi -i color=c=black:s=${videoResolution}:r=${videoFramerate}:d=3 -f lavfi -i anullsrc=channel_layout=stereo:sample_rate=44100 -t ${K_SILENT_DURATION} -c:v libx264 -pix_fmt yuv420p -c:a aac ${BLANK_FILE}
`;

  let clipVideosCmd = "";
  let clipVideosList = "";
  let n = 0;
  for (const t of tsb) {
    if (t === null) {
      clipVideosList += "file '" + BLANK_FILE + "'\n";
      continue;
    }
    n++;
    const filename = prefix + randomString() + ".ts";
    filesToRemove.push(filename);
    clipVideosList += "file '" + filename + "'\n";
    const len = t.end_time - t.start_time;
    clipVideosCmd += `echo "Clipping ${n} of ${ts.length}"
${ffmpeg} -hide_banner -loglevel warning -ss ${t.start_time} -i "$VID" -t ${len} -ss ${t.start_time} -i "$AUD" -t ${len} -c:v libx264 -pix_fmt yuv420p -c:a aac ${filename}\n`;
  }

  let saveListCmd = "";
  const listFilename = prefix + "list.txt";
  filesToRemove.push(listFilename);
  if (platform === "bash") {
    saveListCmd =
      `cat <<EOF | tee ${listFilename} > /dev/null\n` +
      clipVideosList +
      "EOF\n";
  } else if (platform === "powershell") {
    saveListCmd =
      `echo "` +
      clipVideosList +
      `" | Set-Content ${listFilename} -Encoding ASCII`;
  }

  let combineVideosCmd = `echo "Combining videos"
${ffmpeg} -y -hide_banner -loglevel warning -f concat -safe 0 -i ${listFilename} ${outputArguments}`;

  let cleanFilesCmd = 'echo "Cleaning up"\n';
  if (platform === "bash") {
    cleanFilesCmd += `rm ${filesToRemove.join(" ")}`;
  } else if (platform === "powershell") {
    cleanFilesCmd += `rm ${filesToRemove.join(", ")}`;
  }

  const command = `
${info}
${getUrls}
${generateBlank}
${clipVideosCmd}
${saveListCmd}
${combineVideosCmd}
${cleanFilesCmd}
echo "All done!"
`;

  return command;
};

type FormatMap = {
  [key: string]: {
    type: "audio" | "video";
    videoResolution?: string;
    videoFramerate?: number;
  };
};

export const K_FORMAT_MAP: FormatMap = {
  "244": {
    type: "video",
    videoResolution: "854x480",
    videoFramerate: 30,
  },
  "135": {
    type: "video",
    videoResolution: "854x480",
    videoFramerate: 30,
  },
  "397": {
    type: "video",
    videoResolution: "854x480",
    videoFramerate: 30,
  },
  "333": {
    type: "video",
    videoResolution: "854x480",
    videoFramerate: 60,
  },

  "247": {
    type: "video",
    videoResolution: "1280x720",
    videoFramerate: 30,
  },
  "136": {
    type: "video",
    videoResolution: "1280x720",
    videoFramerate: 30,
  },
  "302": {
    type: "video",
    videoResolution: "1280x720",
    videoFramerate: 60,
  },
  "298": {
    type: "video",
    videoResolution: "1280x720",
    videoFramerate: 60,
  },
  "398": {
    type: "video",
    videoResolution: "1280x720",
    videoFramerate: 60,
  },
  "334": {
    type: "video",
    videoResolution: "1280x720",
    videoFramerate: 60,
  },

  "248": {
    type: "video",
    videoResolution: "1920x1080",
    videoFramerate: 30,
  },
  "137": {
    type: "video",
    videoResolution: "1920x1080",
    videoFramerate: 30,
  },
  "303": {
    type: "video",
    videoResolution: "1920x1080",
    videoFramerate: 60,
  },
  "299": {
    type: "video",
    videoResolution: "1920x1080",
    videoFramerate: 60,
  },
  "399": {
    type: "video",
    videoResolution: "1920x1080",
    videoFramerate: 60,
  },
  "335": {
    type: "video",
    videoResolution: "1920x1080",
    videoFramerate: 60,
  },
};
