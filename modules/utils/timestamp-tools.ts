import { secondsToTimestampString, TimestampPair } from "./timestampParser";

export type BlankedTimestamps = Array<TimestampPair | null>;

export const blankedTimestampsToString = (ts: BlankedTimestamps): string =>
  !ts || ts.length === 0
    ? "[none]"
    : ts
        .map((pair) =>
          pair === null
            ? "[blank]"
            : `[${secondsToTimestampString(
                pair.start_time
              )} - ${secondsToTimestampString(pair.end_time)}]`
        )
        .join(" ");

export const insertBlanks = (
  ts: TimestampPair[],
  threshold: number
): BlankedTimestamps => {
  const newArr: BlankedTimestamps = [];
  let nBlanks = 0;
  for (let i = 0; i < ts.length; i++) {
    if (i - 1 in ts) {
      if (ts[i].start_time - ts[i - i].end_time > threshold) {
        newArr.push(null);
        nBlanks++;
      }
    }
    newArr.push(ts[i]);
  }
  return newArr;
};

export const padTimestamps = (ts: BlankedTimestamps) => {
  const padded: BlankedTimestamps = [];
  for (let i = 0; i < ts.length; i++) {
    if (!ts[i]) padded.push(null);
    else
      padded.push({
        start_time: Math.max(0, ts[i].start_time - 5),
        end_time: ts[i].end_time + 5,
      });
  }
  return padded;
};

export const mergeTimestamps = (ts: BlankedTimestamps) => {
  for (let i = 0; i < ts.length - 1; i++) {
    while (!!ts[i] && !!ts[i + 1] && ts[i].end_time >= ts[i + 1].start_time) {
      ts[i].end_time = ts[i + 1].end_time;
      ts.splice(i + 1, 1);
    }
  }
  return ts;
};
