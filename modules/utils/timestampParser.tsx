export type TimestampPair = {
  start_time: number;
  end_time: number;
};

const timestampRegex = /\[(\d{1,2}(:\d{2}){1,2})\s?-\s?(\d{1,2}(:\d{2}){1,2})\]/gm;

export const parseTimestampPairs = (
  timestamps: string
): Array<TimestampPair> => {
  const results = Array.from(timestamps.matchAll(timestampRegex));
  return results.map(
    (match): TimestampPair => ({
      start_time: timestampToSeconds(match[1]),
      end_time: timestampToSeconds(match[3]),
    })
  );
};

export const timestampToSeconds = (singleTimestamp: string): number => {
  const split = singleTimestamp.split(":");
  if (split.length === 3) {
    // HH:MM:SS
    return (
      parseInt(split[0]) * 3600 + parseInt(split[1]) * 60 + parseInt(split[2])
    );
  } else if (split.length === 2) {
    // MM:SS
    return parseInt(split[0]) * 60 + parseInt(split[1]);
  } else throw new Error(`Unable to parse timestamp: ${singleTimestamp}`);
};

export const secondsToTimestampString = (totalSeconds: number): string => {
  const seconds = totalSeconds % 60;
  const minutes = Math.floor(totalSeconds / 60) % 60;
  const hours = Math.floor(totalSeconds / 3600);

  const mmss = `${minutes
    .toString()
    .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;
  if (hours > 0) return `${hours}:${mmss}`;
  else return mmss;
};

export const timestampPairsToString = (
  timestamps: Array<TimestampPair>
): string =>
  !timestamps || timestamps.length === 0
    ? ""
    : timestamps
        .map(
          (pair) =>
            `[${secondsToTimestampString(
              pair.start_time
            )} - ${secondsToTimestampString(pair.end_time)}]`
        )
        .join(" ");

export const getTotalLengthFromTimestampPairs = (
  timestamps: Array<TimestampPair>
) =>
  timestamps.reduce<number>(
    (acc, current) => acc + current.end_time - current.start_time,
    0
  );
