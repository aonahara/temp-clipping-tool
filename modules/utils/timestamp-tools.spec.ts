import {
  BlankedTimestamps,
  blankedTimestampsToString,
  insertBlanks,
  mergeTimestamps,
  padTimestamps,
} from "./timestamp-tools";
import { TimestampPair } from "./timestampParser";

describe("timestamps", () => {
  describe("blankedTimestampsToString", () => {
    const tests: BlankedTimestamps[] = [
      [{ start_time: 0, end_time: 10 }, null, { start_time: 15, end_time: 30 }],
      [null],
      [],
    ];
    const expects: string[] = [
      "[00:00 - 00:10] [blank] [00:15 - 00:30]",
      "[blank]",
      "[none]",
    ];

    tests.map((test, i) =>
      it(expects[i], () =>
        expect(blankedTimestampsToString(test)).toStrictEqual(expects[i])
      )
    );
  });

  describe("insertBlanks", () => {
    const tests: TimestampPair[][] = [
      [
        { start_time: 0, end_time: 10 },
        { start_time: 11, end_time: 15 },
      ],
      [
        { start_time: 0, end_time: 5 },
        { start_time: 25, end_time: 35 },
      ],
      [
        { start_time: 0, end_time: 15 },
        { start_time: 25, end_time: 35 },
      ],
    ];
    const expects: BlankedTimestamps[] = [
      [
        { start_time: 0, end_time: 10 },
        { start_time: 11, end_time: 15 },
      ],
      [{ start_time: 0, end_time: 5 }, null, { start_time: 25, end_time: 35 }],
      [
        { start_time: 0, end_time: 15 },
        { start_time: 25, end_time: 35 },
      ],
    ];

    tests.map((test, i) =>
      it(blankedTimestampsToString(test), () =>
        expect(insertBlanks(test, 10)).toStrictEqual(expects[i])
      )
    );
  });

  describe("padTimestamps", () => {
    const tests: BlankedTimestamps[] = [
      [{ start_time: 0, end_time: 10 }],
      [
        { start_time: 10, end_time: 15 },
        { start_time: 17, end_time: 20 },
      ],
      [
        { start_time: 17, end_time: 25 },
        null,
        { start_time: 27, end_time: 30 },
      ],
    ];
    const expects: BlankedTimestamps[] = [
      [{ start_time: 0, end_time: 15 }],
      [
        { start_time: 5, end_time: 20 },
        { start_time: 12, end_time: 25 },
      ],
      [
        { start_time: 12, end_time: 30 },
        null,
        { start_time: 22, end_time: 35 },
      ],
    ];

    tests.map((test, i) =>
      it(blankedTimestampsToString(test), () =>
        expect(padTimestamps(test)).toStrictEqual(expects[i])
      )
    );
  });

  describe("mergeTimestamps", () => {
    const tests: BlankedTimestamps[] = [
      [
        { start_time: 25, end_time: 45 },
        { start_time: 30, end_time: 55 },
        { start_time: 120, end_time: 180 },
      ],
      [
        { start_time: 10, end_time: 20 },
        { start_time: 20, end_time: 30 },
      ],
      [
        { start_time: 5, end_time: 6 },
        { start_time: 6, end_time: 7 },
        { start_time: 7, end_time: 8 },
        { start_time: 8, end_time: 9 },
      ],
    ];
    const expects: BlankedTimestamps[] = [
      [
        { start_time: 25, end_time: 55 },
        { start_time: 120, end_time: 180 },
      ],
      [{ start_time: 10, end_time: 30 }],
      [{ start_time: 5, end_time: 9 }],
    ];

    tests.map((test, i) =>
      it(blankedTimestampsToString(test), () =>
        expect(mergeTimestamps(test)).toStrictEqual(expects[i])
      )
    );
  });
});
